
export default class {
	constructor() {
		this.btn = ".js-toggle";
		this.$btn = $(this.btn);
		this.$root = $("body");
		this.$doc = $(document);
		this._bindEvents();
	}
	_bindEvents() {
		this.$btn.on("click", this._clickAction.bind(this));
		this.$doc.on("click", this._hide.bind(this));
	}
	_clickAction(event) {
		this.$active = $(event.currentTarget);
		this.rootClass = this.$active.data("root-class");
		this.$target = $(this.$active.data("toggle"));
		this.autohide = this.$active.data("autohide");
    this.anim = this.$active.data("anim");
		$('[data-toggle="'+this.$active.data("toggle")+'"]').toggleClass("is-active");
		this.$target.toggleClass("is-active");
		if (this.rootClass) {
			this.$root.toggleClass(this.rootClass);
		}
		if (this.autohide) {
			this.$target.on("click", function(event) {
				event.stopPropagation();
			});
		}
    if (this.anim == 'fade') {
      this.$target.fadeToggle();
    }
    if (this.anim == 'slide') {
      this.$target.slideToggle();
    }
		return false;
	}
	_hide() {
		if (this.autohide) {
			$('[data-toggle="'+this.$active.data("toggle")+'"]').removeClass("is-active");
			this.$target.removeClass("is-active");
			this.autohide = false;
		}
		
	}
}
