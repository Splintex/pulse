export default class {
  constructor(offset) {
    this.$el = $('body');
    this.$win = $(window);
    this.offset = offset;
    this._bindEvents();
    this._makeSticky();
  }
  
  _bindEvents() {
    this.$win.on('scroll', this._makeSticky.bind(this));
    this.$win.on('resize', this._makeSticky.bind(this));
  }
  
  _makeSticky() {
    if (this.$win.scrollTop() >= this.offset) {
      this.$el.addClass('is-sticky-header');
    }
    else {
      this.$el.removeClass('is-sticky-header');
    }
  }
}
