import jquery from 'jquery';
import slick from 'slick-carousel';
import Toggle from './modules/toggle';
import Sticky from './modules/sticky';
import Select from './modules/select';
import svg4everybody from 'svg4everybody';

svg4everybody();

function CustomSelect() {
  if (!$('.js-select').parent().hasClass('sssl')) {
    new Select({
      selector: ".js-select",
      cssClass: "dropdown"
    });
  }
}

CustomSelect();
module.exports = CustomSelect;

new Toggle('.js-toggle');
new Sticky(187);




window.jQuery = jquery;
window.$ = jquery;

$(".js-slider").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  dots: true,
  prevArrow: '<button class="btn-prev" type="button"><i class="icon icon-caret-l"></i></button>',
  nextArrow: '<button class="btn-next" type="button"><i class="icon icon-caret-r"></i></button>',
  responsive: [
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
      }
    }
  ]
});

$('.js-items-slider').slick({
  slidesToShow: 4,
  slidesToScroll: 4,
  dots: true,
  arrows: false,
  adaptiveHeight: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 645,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
$('.js-items-slider-second').slick({
  responsive: [
    {
      breakpoint: 2000,
      settings: 'unslick'
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: true,
        arrows: false
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: true,
        arrows: false
      }
    },
    {
      breakpoint: 630,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: true,
        arrows: false
      }
    },
    {
      breakpoint: 375,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false
      }
    }
  ]
});
$('.js-review-slider').slick({
  dots: true,
  rows: 2,
  slidesPerRow: 1,
  arrows: false,
  responsive: [
    {
      breakpoint: 630,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        rows: 1,
        slidesPerRow: 1
      }
    }
  ]
});

$(".js-categories-slider").slick({
  rows: 3,
  slidesPerRow: 4,
  dots: false,
  arrows: false,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        rows: 2,
        slidesPerRow: 4,
        dots: true
      }
    },
    {
      breakpoint: 620,
      settings: {
        rows: 2,
        slidesPerRow: 2,
        dots: true
      }
    }
  ]
});


$('.js-dl').slick({
  slidesToShow: 5,
  slidesToScroll: 5,
  dots: true,
  arrows: false
});


class Tabs {
  constructor(el) {
    this.tabs = el;
    this.tabLink = el+" a";
    this.init();
    this.bindEvents();
  }
  init() {
    let hash = window.location.hash;
    if (hash) {
      let $tab = $('[href="'+hash+'"]');
      let $tabs = $tab.closest(this.tabs);
      let $group = $('[data-tabs="'+$tabs.data("content")+'"]');
      let $content = $('[data-id="'+hash+'"]');
      $group.hide();
      $content.show();
      $tabs.find("li").removeClass("is-active");
      $tab.parent().addClass("is-active");
    }
    else {
      $(this.tabs).each(function(){
        $(this).find('li:first').addClass("is-active");
        $(this).next().show();
      });
      
    }
  }
  bindEvents() {
    $(this.tabLink).on("click", this.changeTab.bind(this));
  }
  changeTab(event) {
    console.log(this.hash)
    let $tab = $(event.currentTarget);
    let $tabs = $tab.closest(this.tabs);
    let $group = $('[data-tabs="'+$tabs.data("content")+'"]');
    let $content = $('[data-id="'+$tab.attr("href")+'"]')
    $tabs.find("li").removeClass("is-active");
    $tab.parent().addClass("is-active");
    $group.hide();
    $content.show();
    window.location.hash = $tab.attr('href');
    return false;
  }
}
new Tabs(".js-tabs");

(function(){
  var contents = $('.js-accordeon-content');
  var titles = $('.js-accordeon-title');
  titles.on('click',function(e){
    var title = $(this);
    var $active = $(e.currentTarget);
    
    contents.filter(':visible').slideUp(function(){
      $(this).prev('.js-accordeon-title').removeClass('is-opened');
      $('html, body').animate({
        scrollTop: $active.offset().top - 60
      }, 200);
    });
    
    var content = title.next('.js-accordeon-content');
    
    if (!content.is(':visible')) {
      title.addClass('is-opened');
      content.slideDown();
    }
  });
})();